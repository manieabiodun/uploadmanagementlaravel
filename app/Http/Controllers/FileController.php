<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::all();

        return response()->json($files);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        File::create(array('name' => $request->filepond->getClientOriginalName()));
        return  Storage::putFileAs('bucket', $request->filepond, $request->filepond->getClientOriginalName());
     
      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        $oldFileName = $file->name;
        $newFileName = $request->name;
        $file->update(array('name'=>$newFileName));
        Storage::move('bucket/'.$oldFileName, 'bucket/'.$newFileName);
        return response()->json([
            'message' => 'Great success! Filename has being updated',
            'file' => $newFileName
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $oldFileName = $file->name;   
       
        if(Storage::delete('bucket/'.$oldFileName)){
            $file->delete();

            return response()->json([
                'message' => 'Great success! File Deleted'
            ]);
        }
       
    }
}